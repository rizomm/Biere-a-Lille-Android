package com.ldt.bal.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String LOG = "DatabaseHelper";
    public static final String TABLE_COMMENTS_BEERS = "beers";
    public static final String TABLE_COMMENTS_BARS = "bars";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NOM = "nom";
    public static final String COLUMN_DESC_FR = "desc_fr";
    public static final String COLUMN_DESC_EN = "desc_en";
    public static final String COLUMN_DEGRE = "degre";
    public static final String COLUMN_REMOTE_ID = "remote_id";
    public static final String COLUMN_IMAGE = "image";
    private static final String DATABASE_NAME = "favoris.db";
    private static final int DATABASE_VERSION = 16;

    private static final String CREATE_DATABASE_BEERS = "CREATE TABLE "
            + TABLE_COMMENTS_BEERS + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_REMOTE_ID + " TEXT NOT NULL" + " ,"
            + COLUMN_NOM + " TEXT NOT NULL" + " ,"
            + COLUMN_DESC_FR + " TEXT" + " ,"
            + COLUMN_DESC_EN + " TEXT" + " ,"
            + COLUMN_DEGRE + " TEXT" + " ,"
            + COLUMN_IMAGE + " TEXT"
            + ");";

    private static final String CREATE_DATABASE_BARS = "CREATE TABLE "
            + TABLE_COMMENTS_BARS + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_REMOTE_ID + " TEXT NOT NULL" + " ,"
            + COLUMN_NOM + " TEXT NOT NULL"
            + ");";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        Log.d(LOG, "Creating Database : " + CREATE_DATABASE_BEERS);
        Log.d(LOG, "Creating Database : " + CREATE_DATABASE_BARS);
        database.execSQL(CREATE_DATABASE_BEERS);
        database.execSQL(CREATE_DATABASE_BARS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS_BEERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS_BARS);
        onCreate(db);
    }
}
