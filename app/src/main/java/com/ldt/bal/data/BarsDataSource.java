package com.ldt.bal.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.ldt.bal.model.Bar;

import java.util.ArrayList;
import java.util.List;

public class BarsDataSource {
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {DatabaseHelper.COLUMN_ID,
            DatabaseHelper.COLUMN_NOM,
            DatabaseHelper.COLUMN_REMOTE_ID
    };

    public BarsDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Bar createFavori(ContentValues values) {
        long insertId = database.insert(DatabaseHelper.TABLE_COMMENTS_BARS, null,
                values);
        return getBarForId(insertId);
    }


    public Bar getBarForId(long id) {
        Cursor cursor = database.query(DatabaseHelper.TABLE_COMMENTS_BARS,
                allColumns, DatabaseHelper.COLUMN_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            return null;
        }
        Bar newBar = new Bar(cursor);
        cursor.close();
        return newBar;
    }

    public Bar getBarForRemoteId(String remoteId) {
        Cursor cursor = database.query(DatabaseHelper.TABLE_COMMENTS_BARS,
                allColumns, DatabaseHelper.COLUMN_REMOTE_ID + " = " + remoteId, null,
                null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            return null;
        }
        Bar newBar = new Bar(cursor);
        cursor.close();
        return newBar;
    }

    public void deleteFavori(Bar bar) {
        database.delete(DatabaseHelper.TABLE_COMMENTS_BARS, DatabaseHelper.COLUMN_REMOTE_ID
                + " = " + bar.getId(), null);
    }

    public List<Bar> getAll() {
        List<Bar> bars = new ArrayList<Bar>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_COMMENTS_BARS, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Bar bar = new Bar(cursor);
            bars.add(bar);
            cursor.moveToNext();
        }

        cursor.close();
        return bars;
    }
}
