package com.ldt.bal.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.ldt.bal.model.Beer;

import java.util.ArrayList;
import java.util.List;

public class BeersDataSource {
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {DatabaseHelper.COLUMN_ID,
            DatabaseHelper.COLUMN_NOM,
            DatabaseHelper.COLUMN_REMOTE_ID,
            DatabaseHelper.COLUMN_DEGRE,
            DatabaseHelper.COLUMN_DESC_FR,
            DatabaseHelper.COLUMN_DESC_EN,
            DatabaseHelper.COLUMN_IMAGE
    };

    public BeersDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Beer createFavori(ContentValues values) {
        long insertId = database.insert(DatabaseHelper.TABLE_COMMENTS_BEERS, null,
                values);
        return getBeerForId(insertId);
    }


    public Beer getBeerForId(long id) {
        Cursor cursor = database.query(DatabaseHelper.TABLE_COMMENTS_BEERS,
                allColumns, DatabaseHelper.COLUMN_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            return null;
        }
        Beer newBeer = new Beer(cursor);
        cursor.close();
        return newBeer;
    }

    public Beer getBeerForRemoteId(String remoteId) {
        Cursor cursor = database.query(DatabaseHelper.TABLE_COMMENTS_BEERS,
                allColumns, DatabaseHelper.COLUMN_REMOTE_ID + " = " + remoteId, null,
                null, null, null);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            return null;
        }
        Beer newBeer = new Beer(cursor);
        cursor.close();
        return newBeer;
    }

    public void deleteFavori(Beer beer) {
        database.delete(DatabaseHelper.TABLE_COMMENTS_BEERS, DatabaseHelper.COLUMN_REMOTE_ID
                + " = " + beer.getId(), null);
    }

    public List<Beer> getAll() {
        List<Beer> beers = new ArrayList<Beer>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_COMMENTS_BEERS, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Beer beer = new Beer(cursor);
            beers.add(beer);
            cursor.moveToNext();
        }

        cursor.close();
        return beers;
    }
}
