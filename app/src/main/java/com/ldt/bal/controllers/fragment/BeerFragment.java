package com.ldt.bal.controllers.fragment;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ldt.bal.R;
import com.ldt.bal.controllers.activity.DetailBeerActivity;
import com.ldt.bal.controllers.adapter.BeerCustomListAdapter;
import com.ldt.bal.data.BeersDataSource;
import com.ldt.bal.model.Beer;
import com.ldt.bal.services.restservices.BeerService;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class BeerFragment extends Fragment {
    public final static String EXTRA_TITLE_BEER = "title";
    public final static String EXTRA_ID_BEER = "id";
    public final static String EXTRA_DEGREE_BEER = "degree";
    public final static String EXTRA_DESCRIPTION_FR_BEER = "descriptionFr";
    public final static String EXTRA_DESCRIPTION_EN_BEER = "descriptionEn";
    public final static String EXTRA_PICTURE_BEER = "image";

    private List<Beer> beers = new ArrayList<Beer>();
    private final ArrayList<String> listNom = new ArrayList<String>();
    private AsyncTask<BeerService, Integer, List<String>> contentLoadasyncTask;
    private ListView listView;
    private BeerCustomListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_beer, container, false);
        listView = (ListView) rootView.findViewById(android.R.id.list);
        mAdapter = new BeerCustomListAdapter(getActivity(), android.R.id.list, beers);
        setListItemClick();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView.setAdapter(mAdapter);
        refreshList();
        reloadContentAll();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void reloadContent(final String search) {

        contentLoadasyncTask = new AsyncTask<BeerService, Integer, List<String>>() {
            @Override
            protected List<String> doInBackground(BeerService... params) {

                try {
                    beers = params[0].listBeers(BeerService.CMD_GET_BY_SEARCH, search);
                } catch (RetrofitError e) {
                    e.printStackTrace();
                    return null;
                }

                listNom.clear();
                for (Beer beer : beers) {
                    listNom.add(beer.getNom());
                }

                return listNom;
            }

            @Override
            protected void onPostExecute(List<String> beersNoms) {
                super.onPostExecute(beersNoms);
                if (beersNoms == null) {
                    if (isAdded()) {
                        //Toast.makeText(getActivity(), R.string.error_serv, Toast.LENGTH_LONG).show();
                    }
                } else {
                    refreshList();
                }
            }
        };
        contentLoadasyncTask.execute(new BeerService[]{getBeerService()});
    }

    public void reloadContentAll() {

        contentLoadasyncTask = new AsyncTask<BeerService, Integer, List<String>>() {
            @Override
            protected List<String> doInBackground(BeerService... params) {

                try {
                    beers = params[0].listBeersAll();
                } catch (RetrofitError e) {
                    e.printStackTrace();
                    return null;
                }

                listNom.clear();
                for (Beer beer : beers) {
                    listNom.add(beer.getNom());
                }

                return listNom;
            }

            @Override
            protected void onPostExecute(List<String> beersNoms) {
                super.onPostExecute(beersNoms);
                if (beersNoms == null) {
                    if (isAdded()) {
                        //Toast.makeText(getActivity(), R.string.error_serv, Toast.LENGTH_LONG).show();
                    }
                } else {
                    refreshList();
                }
            }
        };
        contentLoadasyncTask.execute(new BeerService[]{getBeerService()});
    }

    private void setListItemClick() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                final Beer beerItem = mAdapter.getItem(position);

                Intent intent = new Intent(getActivity(), DetailBeerActivity.class);
                intent.putExtra(EXTRA_TITLE_BEER, beerItem.getNom());
                intent.putExtra(EXTRA_ID_BEER, beerItem.getId());
                intent.putExtra(EXTRA_DEGREE_BEER, beerItem.getDegre());
                intent.putExtra(EXTRA_DESCRIPTION_FR_BEER, beerItem.getDescriptionFr());
                intent.putExtra(EXTRA_DESCRIPTION_EN_BEER, beerItem.getDescriptionEn());
                intent.putExtra(EXTRA_PICTURE_BEER, beerItem.getImage());
                startActivity(intent);
            }
        });
    }

    private BeerService getBeerService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer("http://biere-a-lille.ovh")
                .build();
        BeerService beerService = restAdapter.create(BeerService.class);
        return beerService;
    }

    public void switchFavori(View view) {
        int position = listView.getPositionForView(view);
        Beer beerItem = mAdapter.getItem(position);


        BeersDataSource dataSource = new BeersDataSource(getActivity());
        dataSource.open();

        if ((dataSource.getBeerForRemoteId(beerItem.getId())) == null)
        {
            dataSource.close();
            beerItem.saveToDatabase(getActivity());
            Toast.makeText(getActivity(), R.string.succes_fav, Toast.LENGTH_SHORT).show();
        } else
        {
            dataSource.close();
            beerItem.delete(this.getActivity());
            Toast.makeText(getActivity(), R.string.del_fav, Toast.LENGTH_SHORT).show();
        }
        dataSource.close();
        refreshList();
    }

    public void refreshList() {
        mAdapter.clear();
        mAdapter.addAll(beers);
        mAdapter.notifyDataSetChanged();
    }

    public void setfilterList(String filter)
    {
        contentLoadasyncTask.cancel(true);
        if (filter.isEmpty()) {
            reloadContentAll();
        } else {
            reloadContent(filter);
        }
        refreshList();
    }

    public void updateLocation(Location location) {
        mAdapter.setLocate(true);
        mAdapter.setLocation(location);
        refreshList();
    }
}