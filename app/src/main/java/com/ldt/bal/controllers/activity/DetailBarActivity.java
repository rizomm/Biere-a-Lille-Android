package com.ldt.bal.controllers.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldt.bal.R;
import com.ldt.bal.controllers.adapter.BeerCustomListAdapter;
import com.ldt.bal.controllers.fragment.BarFragment;
import com.ldt.bal.controllers.fragment.BeerFragment;
import com.ldt.bal.data.BarsDataSource;
import com.ldt.bal.model.Bar;
import com.ldt.bal.model.Beer;
import com.ldt.bal.services.restservices.BeerService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class DetailBarActivity extends Activity {
    private Bar bar;
    private AsyncTask<BeerService, Integer, List<String>> contentLoadasyncTask;
    private List<Beer> beers = new ArrayList<Beer>();
    private final ArrayList<String> listNom = new ArrayList<String>();
    private ListView listView;
    private BeerCustomListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_details);

        Intent intent = getIntent();
        TextView title = (TextView) findViewById(R.id.title_bar);
        TextView address = (TextView) findViewById(R.id.address_bar);
        TextView description = (TextView) findViewById(R.id.description_bar);
        ImageView imageView = (ImageView) findViewById(R.id.img_bar);

        if (intent != null) {
            bar = new Bar(
                    intent.getStringExtra(BarFragment.EXTRA_TITLE_BAR),
                    intent.getStringExtra(BarFragment.EXTRA_ADRESSE_BAR),
                    intent.getStringExtra(BarFragment.EXTRA_LATITUDE_BAR),
                    intent.getStringExtra(BarFragment.EXTRA_LONGITUDE_BAR),
                    intent.getStringExtra(BarFragment.EXTRA_DESCRIPTION_FR_BAR),
                    intent.getStringExtra(BarFragment.EXTRA_DESCRIPTION_EN_BAR),
                    intent.getStringExtra(BarFragment.EXTRA_IMAGE_BAR),
                    intent.getStringExtra(BarFragment.EXTRA_ID_BAR)
            );
        }
        title.setText(bar.getNom());
        address.setText(bar.getAdresse());

        listView = (ListView) findViewById(R.id.list_beer);
        mAdapter = new BeerCustomListAdapter(this, R.id.list_beer, beers);
        listView.setAdapter(mAdapter);
        setListItemClick();

        if (Locale.getDefault().getLanguage().equals("fr")) {
            description.setText(bar.getDescription_fr());
        } else {
            description.setText(bar.getDescription_en());
        }

        Log.d("IMAGE", bar.getImage());
        if (!bar.getImage().isEmpty()) {
            Picasso.with(getBaseContext()).load(bar.getImage()).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.no_img);
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        hideAddButton();
        reloadContent();
        refreshList();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void reloadContent() {

        contentLoadasyncTask = new AsyncTask<BeerService, Integer, List<String>>() {
            @Override
            protected List<String> doInBackground(BeerService... params) {

                try {
                    beers = params[0].listBeers(BeerService.CMD_GET_BY_BAR, bar.getId());
                } catch (RetrofitError e) {
                    e.printStackTrace();
                    return null;
                }

                listNom.clear();
                if (beers != null) {
                    for (Beer beer : beers) {
                        listNom.add(beer.getNom());
                    }
                }

                return listNom;
            }

            @Override
            protected void onPostExecute(List<String> beersNoms) {
                super.onPostExecute(beersNoms);
                if (beersNoms != null) {
                    refreshList();
                }
            }
        };
        contentLoadasyncTask.execute(new BeerService[]{getBeerService()});
    }

    private BeerService getBeerService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer("http://biere-a-lille.ovh")
                .build();
        BeerService beerService = restAdapter.create(BeerService.class);
        return beerService;
    }

    public void refreshList() {
        if (beers != null) {
            mAdapter.clear();
            mAdapter.addAll(beers);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setListItemClick() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                final Beer beerItem = mAdapter.getItem(position);

                Intent intent = new Intent(getBaseContext(), DetailBeerActivity.class);
                intent.putExtra(BeerFragment.EXTRA_TITLE_BEER, beerItem.getNom());
                intent.putExtra(BeerFragment.EXTRA_ID_BEER, beerItem.getId());
                intent.putExtra(BeerFragment.EXTRA_DEGREE_BEER, beerItem.getDegre());
                intent.putExtra(BeerFragment.EXTRA_DESCRIPTION_FR_BEER, beerItem.getDescriptionFr());
                intent.putExtra(BeerFragment.EXTRA_DESCRIPTION_EN_BEER, beerItem.getDescriptionEn());
                intent.putExtra(BeerFragment.EXTRA_PICTURE_BEER, beerItem.getImage());
                startActivity(intent);
            }
        });
    }

    private void hideAddButton() {
//        View view = getWindow().getDecorView();
//        BarsDataSource dataSource = new BarsDataSource(this);
//        dataSource.open();
//        if ((dataSource.getBarForRemoteId(bar.getId())) != null) {
//            ImageView add = (ImageView) findViewById(R.id.favori);
//            add.setVisibility(view.GONE);
//            Button del = (Button) findViewById(R.id.deleteFav);
//            del.setVisibility(1);
//        } else {
//            ImageView add = (ImageView) findViewById(R.id.fav_image_bar);
//            add.setVisibility(1);
//            Button del = (Button) findViewById(R.id.deleteFav);
//            del.setVisibility(view.GONE);
//        }
//        dataSource.close();
    }

    public void onClickExpo(View view) {
        switch (view.getId()) {
            case R.id.favori:
                addToFavori(view);
                break;
            case R.id.deleteFav:
                deleteFavoris(view);
                break;
        }
    }

    private void addToFavori(View view) {
        BarsDataSource dataSource = new BarsDataSource(this);
        dataSource.open();
        if ((dataSource.getBarForRemoteId(bar.getId())) == null) {
            dataSource.close();
            bar.saveToDatabase(this);
            Toast.makeText(this, R.string.succes_fav, Toast.LENGTH_LONG).show();
            Button add = (Button) findViewById(R.id.favori);
            add.setVisibility(view.GONE);
            Button del = (Button) findViewById(R.id.deleteFav);
            del.setVisibility(1);
        } else {
            dataSource.close();
            Toast.makeText(this, R.string.already_fav, Toast.LENGTH_LONG).show();
        }
    }

    private void deleteFavoris(View view) {
        BarsDataSource dataSource2 = new BarsDataSource(this);
        dataSource2.open();
        if ((dataSource2.getBarForRemoteId(bar.getId())) != null) {
            dataSource2.close();
            bar.delete(this);
            Toast.makeText(this, R.string.del_fav, Toast.LENGTH_LONG).show();
            Button add = (Button) findViewById(R.id.favori);
            add.setVisibility(1);
            Button del = (Button) findViewById(R.id.deleteFav);
            del.setVisibility(view.GONE);
        }
    }

}
