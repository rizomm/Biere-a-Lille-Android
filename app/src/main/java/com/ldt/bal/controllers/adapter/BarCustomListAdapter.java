package com.ldt.bal.controllers.adapter;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldt.bal.R;
import com.ldt.bal.model.Bar;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BarCustomListAdapter extends ArrayAdapter<Bar> {
    private List<Bar> bars;
    private Context context;
    private ViewHolder holder = null;
    private Location location;
    private boolean isLocate;

    public BarCustomListAdapter(Context context, int textViewResourceId, List<Bar> objects) {
        super(context, textViewResourceId, objects);
        this.bars = objects;
        this.context = context;
        this.isLocate = false;
    }

    private static class ViewHolder {
        ImageView beerView;
        ImageView favView;
        TextView txtMenuName;
        TextView txtMenuDesc;
        TextView txtDistance;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        Bar rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_bars, null);
            holder = new ViewHolder();
            holder.txtMenuName = (TextView) convertView.findViewById(R.id.list_title);
            holder.txtMenuDesc = (TextView) convertView.findViewById(R.id.list_address);
            holder.txtDistance = (TextView) convertView.findViewById(R.id.list_distance);
//            holder.favView = (ImageView) convertView.findViewById(R.id.fav_image);
            holder.beerView = (ImageView) convertView.findViewById(R.id.beer_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtMenuDesc.setText(rowItem.getAdresse());
        holder.txtMenuName.setText(rowItem.getNom());

        if (isLocate)
        {
            holder.txtDistance.setText(getDistance(rowItem));
        }

      //  BeersDataSource dataSource = new BeersDataSource(getContext());
      //  dataSource.open();

      //  if ((dataSource.getBeerForRemoteId(rowItem.getId())) == null) {
       //     holder.favView.setImageResource(R.drawable.btn_favoris_inactive);
       // } else {
        //    holder.favView.setImageResource(R.drawable.btn_favoris_active);
       // }
      //  dataSource.close();


        if (!rowItem.getImage().isEmpty()) {
            Picasso.with(context).load(rowItem.getImage()).into(holder.beerView);
        } else {
            holder.beerView.setImageResource(R.drawable.no_img);
        }

        return convertView;
    }

    private String getDistance(Bar bar) {

        Location locationBar = new Location("me");
        locationBar.setLatitude(Double.parseDouble(bar.getLatitude()));
        locationBar.setLongitude(Double.parseDouble(bar.getLongitude()));

        float distance = location.distanceTo(locationBar);

        return String.format(context.getString(R.string.distance_to), distance);
    }

    public void setLocate(boolean isLocate) {
        this.isLocate = isLocate;
    }
}