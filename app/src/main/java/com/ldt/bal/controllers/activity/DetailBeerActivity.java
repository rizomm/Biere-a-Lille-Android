package com.ldt.bal.controllers.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldt.bal.R;
import com.ldt.bal.controllers.fragment.BeerFragment;
import com.ldt.bal.data.BeersDataSource;
import com.ldt.bal.model.Beer;
import com.squareup.picasso.Picasso;

import java.util.Locale;

public class DetailBeerActivity extends Activity {
    private Beer beer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beer_detail);

        Intent intent = getIntent();
        TextView title = (TextView) findViewById(R.id.title_beer);
        TextView degree = (TextView) findViewById(R.id.degree_beer);
        TextView description = (TextView) findViewById(R.id.description_berr);

        ImageView expoImg = (ImageView) findViewById(R.id.img_beer);

        if (intent != null) {
            beer = new Beer(
                    intent.getStringExtra(BeerFragment.EXTRA_TITLE_BEER),
                    intent.getStringExtra(BeerFragment.EXTRA_ID_BEER),
                    intent.getStringExtra(BeerFragment.EXTRA_DEGREE_BEER),
                    intent.getStringExtra(BeerFragment.EXTRA_DESCRIPTION_FR_BEER),
                    intent.getStringExtra(BeerFragment.EXTRA_DESCRIPTION_EN_BEER),
                    intent.getStringExtra(BeerFragment.EXTRA_PICTURE_BEER)
            );
        }
        title.setText(beer.getNom());
        degree.setText(beer.getDegre());

        if (Locale.getDefault().getLanguage().equals("fr")) {
            description.setText(beer.getDescriptionFr());
        } else {
            description.setText(beer.getDescriptionEn());
        }

        String photo = "http://" + beer.getImage();
        if (!beer.getImage().isEmpty()) {
            Picasso.with(getBaseContext()).load(photo).into(expoImg);
        } else {
            expoImg.setImageResource(R.drawable.no_img);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideAddButton();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void hideAddButton() {
        View view = getWindow().getDecorView();
        BeersDataSource dataSource = new BeersDataSource(this);
        dataSource.open();
/*        if ((dataSource.getBeerForRemoteId(beer.getId())) != null) {
            Button add = (Button) findViewById(R.id.favori);
            add.setVisibility(view.GONE);
            Button del = (Button) findViewById(R.id.deleteFav);
            del.setVisibility(1);
        } else {
            Button add = (Button) findViewById(R.id.favori);
            add.setVisibility(1);
            Button del = (Button) findViewById(R.id.deleteFav);
            del.setVisibility(view.GONE);
        }*/
        dataSource.close();
    }

    public void onClickExpo(View view) {
        switch (view.getId()) {
            case R.id.favori:
                addToFavori(view);
                break;
            case R.id.deleteFav:
                deleteFavoris(view);
                break;
        }
    }

    private void addToFavori(View view) {
        BeersDataSource dataSource = new BeersDataSource(this);
        dataSource.open();
        if ((dataSource.getBeerForRemoteId(beer.getId())) == null) {
            dataSource.close();
            beer.saveToDatabase(this);
            Toast.makeText(this, R.string.succes_fav, Toast.LENGTH_LONG).show();
            Button add = (Button) findViewById(R.id.favori);
            add.setVisibility(view.GONE);
            Button del = (Button) findViewById(R.id.deleteFav);
            del.setVisibility(1);
        } else {
            dataSource.close();
            Toast.makeText(this, R.string.already_fav, Toast.LENGTH_LONG).show();
        }
    }

    private void deleteFavoris(View view) {
        BeersDataSource dataSource = new BeersDataSource(this);
        dataSource.open();
        if ((dataSource.getBeerForRemoteId(beer.getId())) != null) {
            dataSource.close();
            beer.delete(this);
            Toast.makeText(this, R.string.del_fav, Toast.LENGTH_LONG).show();
            Button add = (Button) findViewById(R.id.favori);
            add.setVisibility(1);
            Button del = (Button) findViewById(R.id.deleteFav);
            del.setVisibility(view.GONE);
        }
    }

}
