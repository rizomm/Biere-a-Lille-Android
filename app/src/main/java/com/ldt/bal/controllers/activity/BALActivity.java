package com.ldt.bal.controllers.activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.ldt.bal.R;
import com.ldt.bal.controllers.adapter.FragmentPagerCustomAdapter;
import com.ldt.bal.controllers.event.LocationEvent;
import com.ldt.bal.controllers.fragment.BarFragment;
import com.ldt.bal.controllers.fragment.BeerFragment;
import com.ldt.bal.controllers.fragment.FavorisFragment;
import com.ldt.bal.services.location.LocationService;

import de.greenrobot.event.EventBus;

public class BALActivity extends FragmentActivity implements SearchView.OnQueryTextListener {
    private ActionBar actionabar;
    private ViewPager viewpager;
    private FragmentPagerCustomAdapter fragmentPagerAdapter;
    private Menu menu;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bal);

        actionabar = getActionBar();
        actionabar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        viewpager = (ViewPager) findViewById(R.id.pager);

        FragmentManager fm = getSupportFragmentManager();

        ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                actionabar.setSelectedNavigationItem(position);
            }
        };

        viewpager.setOnPageChangeListener(pageChangeListener);
        fragmentPagerAdapter = new FragmentPagerCustomAdapter(fm);
        viewpager.setAdapter(fragmentPagerAdapter);
        actionabar.setDisplayShowTitleEnabled(true);
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {

            @Override
            public void onTabReselected(Tab arg0, android.app.FragmentTransaction arg1) {

            }

            @Override
            public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {

            }
        };

        LocationService locationService = new LocationService(this);
        EventBus.getDefault().register(this);

        createTabBar(tabListener);
    }

    public void onEvent(LocationEvent event){
        Log.d("LOCATIONEVENT", "New location" + event.location.getLatitude() + " ," + event.location.getLongitude());

        //Fragment fragmentByTagBeer = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 0));
        Fragment fragmentByTagBar = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 1));
        //((BeerFragment) fragmentByTagBeer).updateLocation(event.location);
        ((BarFragment) fragmentByTagBar).updateLocation(event.location);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(this);
        searchView.clearFocus();
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                getAndReloadFragment();
                break;
        }
        return true;
    }

    private void getAndReloadFragment() {
        Fragment fragmentByTagBeer = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 0));
        Fragment fragmentByTagBar = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 1));
        ((BeerFragment) fragmentByTagBeer).reloadContentAll();
        ((BarFragment) fragmentByTagBar).reloadContentAll();
    }

    private void getAndReloadFavoriFragment() {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 2));
        if (fragmentByTag instanceof FavorisFragment) {
            ((FavorisFragment) fragmentByTag).refreshList();
        }
    }

    private static String makeFragmentName(int viewId, int position) {
        return "android:switcher:" + viewId + ":" + position;
    }

    public void onClickBeerFavoriIcon(View view) {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 0));
        if (fragmentByTag instanceof BeerFragment) {
            ((BeerFragment) fragmentByTag).switchFavori(view);
            getAndReloadFavoriFragment();
        }
    }

    public void onClickExpoFavoriIcon(View view) {
  //      Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 0));
  //      if (fragmentByTag instanceof BeerFragment) {
   //         ((ExpoFragment) fragmentByTag).switchFavori(view);
    //        getAndReloadFavoriFragment();
     //   }
    }

    private void createTabBar(ActionBar.TabListener tabListener) {
        Tab tab = actionabar.newTab().setText(R.string.tab_1).setTabListener(tabListener);
        actionabar.addTab(tab);
        tab = actionabar.newTab().setText(R.string.tab_2).setTabListener(tabListener);
        actionabar.addTab(tab);
        tab = actionabar.newTab().setText(R.string.tab_3).setTabListener(tabListener);
        actionabar.addTab(tab);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d("SEARCH", newText);

        Fragment fragmentByTagBeer = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 0));
        Fragment fragmentByTagBar = getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.pager, 1));
         ((BeerFragment) fragmentByTagBeer).setfilterList(newText);
        ((BarFragment) fragmentByTagBar).setfilterList(newText);
        return false;
    }
}
