package com.ldt.bal.controllers.fragment;
import android.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.ldt.bal.controllers.activity.DetailBarActivity;
import com.ldt.bal.controllers.activity.DetailBeerActivity;
import com.ldt.bal.controllers.adapter.FavoriCustomExpandableAdapter;
import com.ldt.bal.model.Bar;
import com.ldt.bal.model.Beer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FavorisFragment extends ListFragment {
    private List<Beer> beers;
    private List<Bar> bars;
    private ArrayList<String> groupList;
    private Map<String, List<Beer>> beersCollection;
    private Map<String, List<Bar>> barsCollection;
    private ExpandableListView expListView;
    private FavoriCustomExpandableAdapter expListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(com.ldt.bal.R.layout.fragment_favori, container, false);
        beers = Beer.getAllBeers(getActivity());
        bars = Bar.getAllBars(getActivity());
        createGroupList();
        createCollection();
        expListAdapter = new FavoriCustomExpandableAdapter(getActivity(), groupList, beersCollection, barsCollection);
        expListView = (ExpandableListView) rootView.findViewById(R.id.list);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        expListView.setAdapter(expListAdapter);
        setOnClick();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
        setHasOptionsMenu(false);
    }

    public void refreshList() {
        beers.clear();
        beers.addAll(Beer.getAllBeers(getActivity()));
        bars.clear();
        bars.addAll(Bar.getAllBars(getActivity()));
        expListAdapter.notifyDataSetChanged();
    }

    private void createGroupList() {
        groupList = new ArrayList<String>();
        groupList.add("Biéres");
        groupList.add("Bars");
    }

    private void createCollection() {
        beersCollection = new LinkedHashMap<String, List<Beer>>();
        barsCollection = new LinkedHashMap<String, List<Bar>>();

        barsCollection.put("Bars", bars);
        beersCollection.put("Biéres", beers);
    }

    private void setOnClick() {
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                switch (groupPosition) {
                    case 0:
                        final Beer beerSelected = (Beer) expListAdapter.getChild(groupPosition, childPosition);
                        Intent intent = new Intent(getActivity(), DetailBeerActivity.class);
                        intent.putExtra("beer_id", beerSelected.getId());
                        startActivity(intent);
                        break;
                    case 1:
                        final Bar barSelected = (Bar) expListAdapter.getChild(groupPosition, childPosition);
                        Intent intentStock = new Intent(getActivity(), DetailBarActivity.class);
                        intentStock.putExtra("bar_id", barSelected.getId());
                        startActivity(intentStock);
                        break;
                }

                return true;
            }
        });
    }
}


