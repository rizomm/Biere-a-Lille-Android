package com.ldt.bal.controllers.fragment;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ldt.bal.R;
import com.ldt.bal.controllers.activity.DetailBarActivity;
import com.ldt.bal.controllers.adapter.BarCustomListAdapter;
import com.ldt.bal.data.BarsDataSource;
import com.ldt.bal.model.Bar;
import com.ldt.bal.services.restservices.BarService;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class BarFragment extends Fragment {
    public final static String EXTRA_TITLE_BAR = "title";
    public final static String EXTRA_ADRESSE_BAR = "adresse";
    public final static String EXTRA_LATITUDE_BAR = "latitude";
    public final static String EXTRA_LONGITUDE_BAR = "longitude";
    public final static String EXTRA_DESCRIPTION_FR_BAR = "descriptionFr";
    public final static String EXTRA_DESCRIPTION_EN_BAR = "descriptionEn";
    public final static String EXTRA_IMAGE_BAR = "image";
    public final static String EXTRA_ID_BAR = "id";

    private List<Bar> bars = new ArrayList<Bar>();
    private final ArrayList<String> listNom = new ArrayList<String>();
    private AsyncTask<BarService, Integer, List<String>> contentLoadasyncTask;
    private ListView listView;
    private BarCustomListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_beer, container, false);
        listView = (ListView) rootView.findViewById(android.R.id.list);
        mAdapter = new BarCustomListAdapter(getActivity(), android.R.id.list, bars);
        setListItemClick();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView.setAdapter(mAdapter);
        refreshList();
        reloadContentAll();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void reloadContent(final String search) {

        contentLoadasyncTask = new AsyncTask<BarService, Integer, List<String>>() {
            @Override
            protected List<String> doInBackground(BarService... params) {

                try {
                    bars = params[0].listBars(BarService.CMD_GET_BY_BIERE_NAME, search);
                } catch (RetrofitError e) {
                    e.printStackTrace();
                    return null;
                }

                listNom.clear();
                for (Bar beer : bars) {
                    listNom.add(beer.getNom());
                }

                return listNom;
            }

            @Override
            protected void onPostExecute(List<String> beersNoms) {
                super.onPostExecute(beersNoms);
                if (beersNoms == null) {
                    if (isAdded()) {
                        //Toast.makeText(getActivity(), R.string.error_serv, Toast.LENGTH_LONG).show();
                    }
                } else {
                    refreshList();
                }
            }
        };
        contentLoadasyncTask.execute(new BarService[]{getBarService()});
    }

    public void reloadContentAll() {

        contentLoadasyncTask = new AsyncTask<BarService, Integer, List<String>>() {
            @Override
            protected List<String> doInBackground(BarService... params) {

                try {
                    bars = params[0].listBarsAll();
                } catch (RetrofitError e) {
                    e.printStackTrace();
                    return null;
                }

                listNom.clear();
                for (Bar beer : bars) {
                    listNom.add(beer.getNom());
                }

                return listNom;
            }

            @Override
            protected void onPostExecute(List<String> beersNoms) {
                super.onPostExecute(beersNoms);
                if (beersNoms == null) {
                    if (isAdded()) {
                        //Toast.makeText(getActivity(), R.string.error_serv, Toast.LENGTH_LONG).show();
                    }
                } else {
                    refreshList();
                }
            }
        };
        contentLoadasyncTask.execute(new BarService[]{getBarService()});
    }

    private void setListItemClick() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                final Bar barItem = mAdapter.getItem(position);

                Intent intent = new Intent(getActivity(), DetailBarActivity.class);
                intent.putExtra(EXTRA_TITLE_BAR, barItem.getNom());
                intent.putExtra(EXTRA_ADRESSE_BAR, barItem.getAdresse());
                intent.putExtra(EXTRA_LATITUDE_BAR, barItem.getLatitude());
                intent.putExtra(EXTRA_LONGITUDE_BAR, barItem.getLongitude());
                intent.putExtra(EXTRA_ID_BAR, barItem.getId());
                intent.putExtra(EXTRA_DESCRIPTION_FR_BAR, barItem.getDescription_fr());
                intent.putExtra(EXTRA_DESCRIPTION_EN_BAR, barItem.getDescription_en());
                intent.putExtra(EXTRA_IMAGE_BAR, barItem.getImage());
                //Log.d("IMAGE" ,barItem.getImage());
                startActivity(intent);
            }
        });
    }

    private BarService getBarService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer("http://biere-a-lille.ovh")
                .build();
        BarService barService = restAdapter.create(BarService.class);
        return barService;
    }

    public void switchFavori(View view) {
        int position = listView.getPositionForView(view);
        Bar beerItem = mAdapter.getItem(position);


        BarsDataSource dataSource = new BarsDataSource(getActivity());
        dataSource.open();

        if ((dataSource.getBarForRemoteId(beerItem.getId())) == null) {
            dataSource.close();
            beerItem.saveToDatabase(getActivity());
            Toast.makeText(getActivity(), R.string.succes_fav, Toast.LENGTH_SHORT).show();

        } else {
            dataSource.close();
            beerItem.delete(this.getActivity());
            Toast.makeText(getActivity(), R.string.del_fav, Toast.LENGTH_SHORT).show();
        }
        dataSource.close();
        refreshList();
    }

    public void refreshList() {
        mAdapter.clear();
        mAdapter.addAll(bars);
        mAdapter.notifyDataSetChanged();
    }

    public void setfilterList(String filter)
    {
        contentLoadasyncTask.cancel(true);
        if (filter.isEmpty()) {
            reloadContentAll();
        } else {
            reloadContent(filter);
        }
        refreshList();
    }

    public void updateLocation(Location location) {
        mAdapter.setLocate(true);
        mAdapter.setLocation(location);
        refreshList();
    }
}