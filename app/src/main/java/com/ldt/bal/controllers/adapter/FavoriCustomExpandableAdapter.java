package com.ldt.bal.controllers.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.ldt.bal.R;
import com.ldt.bal.model.Bar;
import com.ldt.bal.model.Beer;

import java.util.List;
import java.util.Map;

public class FavoriCustomExpandableAdapter extends BaseExpandableListAdapter {
    private Activity context;
    private Map<String, List<Beer>> beersCollections;
    private Map<String, List<Bar>> barsCollections;
    private List<String> favoris;

    public FavoriCustomExpandableAdapter(Activity context, List<String> favoris, Map<String, List<Beer>> beersCollections, Map<String, List<Bar>> barsCollections) {
        this.context = context;
        this.beersCollections = beersCollections;
        this.barsCollections = barsCollections;
        this.favoris = favoris;
    }

    public Object getChild(int groupPosition, int childPosition) {
        switch (groupPosition) {
            case 0:
                return beersCollections.get(favoris.get(groupPosition)).get(childPosition);
            case 1:
                return barsCollections.get(favoris.get(groupPosition)).get(childPosition);
        }
        return null;
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        switch (groupPosition) {
            case 0:
                final Beer beerItem = (Beer) getChild(groupPosition, childPosition);
                LayoutInflater inflater = context.getLayoutInflater();

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.favori_item, null);
                }

                TextView item = (TextView) convertView.findViewById(R.id.favori_item);

                item.setText(beerItem.getNom());
                return convertView;
            case 1:
                final Bar barItem = (Bar) getChild(groupPosition, childPosition);
                LayoutInflater inflater2 = context.getLayoutInflater();

                if (convertView == null) {
                    convertView = inflater2.inflate(R.layout.favori_item, null);
                }

                TextView item2 = (TextView) convertView.findViewById(R.id.favori_item);

                item2.setText(barItem.getNom());
                return convertView;
        }

        return null;
    }

    public int getChildrenCount(int groupPosition) {
        if ((!beersCollections.isEmpty() || !barsCollections.isEmpty()) && !favoris.isEmpty()) {
            switch (groupPosition) {
                case 0:
                    return beersCollections.get(favoris.get(groupPosition)).size();
                case 1:
                    return barsCollections.get(favoris.get(groupPosition)).size();
            }
        }
        return 0;
    }

    public Object getGroup(int groupPosition) {
        return favoris.get(groupPosition);
    }

    public int getGroupCount() {
        return favoris.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String groupName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_favori_item,
                    null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.tabFav);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(groupName);
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
