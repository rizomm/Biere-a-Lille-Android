package com.ldt.bal.controllers.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ldt.bal.controllers.fragment.BarFragment;
import com.ldt.bal.controllers.fragment.BeerFragment;
import com.ldt.bal.controllers.fragment.FavorisFragment;

public class FragmentPagerCustomAdapter extends FragmentPagerAdapter {
    private final int PAGE_COUNT = 3;

    public FragmentPagerCustomAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {
        Bundle data = new Bundle();
        switch (arg0) {
            case 0:
                BeerFragment beersFragment = new BeerFragment();
                data.putInt("current_page", arg0 + 1);
                beersFragment.setArguments(data);
                return beersFragment;
            case 1:
                BarFragment barFragment = new BarFragment();
                data.putInt("current_page", arg0 + 1);
                barFragment.setArguments(data);
                return barFragment;
            case 2:
                FavorisFragment favorisFragment = new FavorisFragment();
                data.putInt("current_page", arg0 + 1);
                favorisFragment.setArguments(data);
                return favorisFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
