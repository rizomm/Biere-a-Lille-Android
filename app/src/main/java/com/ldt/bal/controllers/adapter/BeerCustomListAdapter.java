package com.ldt.bal.controllers.adapter;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldt.bal.R;
import com.ldt.bal.data.BeersDataSource;
import com.ldt.bal.model.Bar;
import com.ldt.bal.model.Beer;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BeerCustomListAdapter extends ArrayAdapter<Beer> {
    private List<Beer> beers;
    private List<Beer> originalBeers;
    private List<Bar> Bars;
    private Context context;
    private BeersDataSource dataSource;
    private ViewHolder holder = null;
    private Location location;
    private boolean isLocate;

    public BeerCustomListAdapter(Context context, int textViewResourceId, List<Beer> objects) {
        super(context, textViewResourceId, objects);
        this.beers = objects;
        this.originalBeers = beers;
        this.context = context;
        this.dataSource = new BeersDataSource(getContext());
        this.dataSource.open();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.dataSource.close();
    }

    private static class ViewHolder {
        ImageView beerView;
        ImageView favView;
        TextView txtMenuName;
        TextView txtMenuDesc;
        TextView txtMenuDistance;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        Beer rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_beers, null);
            holder = new ViewHolder();
            holder.txtMenuName = (TextView) convertView.findViewById(R.id.list_title);
            holder.txtMenuDesc = (TextView) convertView.findViewById(R.id.list_address);
            holder.txtMenuDistance = (TextView) convertView.findViewById(R.id.list_distance);
//            holder.favView = (ImageView) convertView.findViewById(R.id.fav_image);
            holder.beerView = (ImageView) convertView.findViewById(R.id.beer_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtMenuDesc.setText(rowItem.getDegre());
        holder.txtMenuName.setText(rowItem.getNom());

        if (isLocate)
        {
            holder.txtMenuDistance.setVisibility(1);
            holder.txtMenuDistance.setText(getDistance(rowItem));
        } else {
            holder.txtMenuDistance.setVisibility(0);
        }

     /*   if ((this.dataSource.getBeerForRemoteId(rowItem.getId())) == null) {
            holder.favView.setImageResource(R.drawable.btn_favoris_inactive);
        } else {
            holder.favView.setImageResource(R.drawable.btn_favoris_active);
        } */

        Log.d("IMAGE", rowItem.getImage());
        String photo = "http://" + rowItem.getImage();

        if (!rowItem.getImage().isEmpty()) {
            Picasso.with(context).load(photo).into(holder.beerView);
        } else {
            holder.beerView.setImageResource(R.drawable.no_img);
        }

        return convertView;
    }

    private String getDistance(Beer beer) {

//        Location locationBar = new Location("me");
//        locationBar.setLatitude(Double.parseDouble(bar.getLatitude()));
//        locationBar.setLongitude(Double.parseDouble(bar.getLongitude()));
//
//        float distance = location.distanceTo(locationBar);
//
//        return Float.toString(distance);
        return null;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setLocate(boolean isLocate) {
        this.isLocate = isLocate;
    }
}