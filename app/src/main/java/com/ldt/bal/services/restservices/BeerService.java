package com.ldt.bal.services.restservices;

import com.ldt.bal.model.Beer;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

public interface BeerService
{
    public static final String CMD_GET_BY_BAR       = "getbybar";
    public static final String CMD_GET_BY_MARQUE    = "getbymarque";
    public static final String CMD_GET_BY_COULEUR   = "getbycouleur";
    public static final String CMD_GET_BY_SEARCH    = "rechercher";

    @GET("/biere/{cmd}/{search}")
      List<Beer> listBeers(
                @Path("cmd")    String      cmd,
                @Path("search") String      search
        );

    @GET("/biere/all")
    List<Beer> listBeersAll();
}
