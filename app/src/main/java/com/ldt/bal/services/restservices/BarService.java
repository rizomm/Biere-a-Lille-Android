package com.ldt.bal.services.restservices;

import com.ldt.bal.model.Bar;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

public interface BarService
{
    public static final String CMD_GET_BY_BIERE_NAME    = "getByBiereName";
    public static final String CMD_GET_BY_BIERE_ID      = "getByBiereId";
    public static final String CMD_GET_BY_SEARCH        = "rechercher";

    @GET("/bar/{cmd}/{search}")
    List<Bar> listBars(
            @Path("cmd")    String      cmd,
            @Path("search") String      search
    );

    @GET("/bar/all")
    List<Bar> listBarsAll();
}
