package com.ldt.bal.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateHelper {
    public static final String PATTERN = "yyyy-MM-dd'T'hh:mm:ss.000'Z'";
    private final DateFormat dateFormat = new SimpleDateFormat(PATTERN);

    public DateHelper() {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    public Date parse(String s) throws ParseException {
        if (s == null) return null;
        return dateFormat.parse(s);
    }
}
