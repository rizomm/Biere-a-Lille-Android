package com.ldt.bal.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ldt.bal.data.BarsDataSource;
import com.ldt.bal.data.DatabaseHelper;

import java.util.List;

public class Bar {
    private String nom;
    private String adresse;
    private String latitude;
    private String longitude;
    private String description_fr;
    private String description_en;
    private String image;
    private String id;
    private Long _id;

    public Bar(Cursor cursor) {
        this._id = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID));
        this.setNom(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NOM)));
        this.setId(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_REMOTE_ID)));
    }

    public Bar(String nom, String adresse, String latitude, String longitude, String description_fr, String description_en, String image, String id) {
        this.nom = nom;
        this.adresse = adresse;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description_fr = description_fr;
        this.description_en = description_en;
        this.image = image;
        this.id = id;
    }

    @Override
    public String toString() {
        return nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription_fr() {
        return description_fr;
    }

    public void setDescription_fr(String description_fr) {
        this.description_fr = description_fr;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public String getNom() {
        return nom;
    }

    public Long get_id() {
        return _id;
    }

    public String getId() {
        return id;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void saveToDatabase(Context context) {
        if (get_id()!= null) {
            BarsDataSource dataSource = new BarsDataSource(context);
            dataSource.open();
            if (dataSource.getBarForRemoteId(getId()) == null) {
                dataSource.createFavori(getContentValues());
            }
            dataSource.close();
        }
    }

    private ContentValues getContentValues() {

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_NOM, getNom());
        values.put(DatabaseHelper.COLUMN_REMOTE_ID, getId());
        return values;
    }

    public void delete(Context context) {
        BarsDataSource dataSource = new BarsDataSource(context);
        dataSource.open();
        dataSource.deleteFavori(this);
        dataSource.close();
    }

    public static List<Bar> getAllBars(Context context) {
        BarsDataSource barsDataSource = new BarsDataSource(context);
        barsDataSource.open();
        List<Bar> bars = barsDataSource.getAll();
        barsDataSource.close();
        return bars;
    }
}
