package com.ldt.bal.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ldt.bal.data.BeersDataSource;
import com.ldt.bal.data.DatabaseHelper;

import java.util.List;

public class Beer {
    private String nom;
    private String id;
    private String degre;
    private String descriptionFr;
    private String descriptionEn;
    private String image;
    private Long _id;

    public Beer(Cursor cursor) {
        this._id = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID));
        this.setNom(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NOM)));
        this.setDegre(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DEGRE)));
        this.setDescriptionFr(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DESC_FR)));
        this.setDescriptionEn(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DESC_EN)));
        this.setId(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_REMOTE_ID)));
        this.setImage(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_IMAGE)));
    }

    public Beer(String nom, String id, String degre, String descriptionFr, String descriptionEn, String image) {
        this.nom = nom;
        this.id = id;
        this.descriptionFr = descriptionFr;
        this.descriptionEn = descriptionEn;
        this.degre = degre;
        this.image = image;
    }

    @Override
    public String toString() {
        return nom;
    }

    public String getNom() {
        return nom;
    }

    public String getDegre() {
        return degre;
    }

    public String getDescriptionFr() {
        return descriptionFr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public Long get_id() {
        return _id;
    }

    public String getImage() {
        return image;
    }

    public String getId() {
        return id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDegre(String degre) {
        this.degre = degre;
    }

    public void setDescriptionFr(String descriptionFr) {
        this.descriptionFr = descriptionFr;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public void saveToDatabase(Context context) {
        if (get_id()!= null) {
            BeersDataSource dataSource = new BeersDataSource(context);
            dataSource.open();
            if (dataSource.getBeerForRemoteId(getId()) == null) {
                dataSource.createFavori(getContentValues());
            }
            dataSource.close();
        }
    }

    private ContentValues getContentValues() {

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_NOM, getNom());
        values.put(DatabaseHelper.COLUMN_DEGRE, getDegre());
        values.put(DatabaseHelper.COLUMN_DESC_FR, getDescriptionFr());
        values.put(DatabaseHelper.COLUMN_DESC_EN, getDescriptionEn());
        values.put(DatabaseHelper.COLUMN_REMOTE_ID, getId());
        values.put(DatabaseHelper.COLUMN_IMAGE, getImage());
        return values;
    }

    public void delete(Context context) {
        BeersDataSource dataSource = new BeersDataSource(context);
        dataSource.open();
        dataSource.deleteFavori(this);
        dataSource.close();
    }

    public static List<Beer> getAllBeers(Context context) {
        BeersDataSource beersDataSource = new BeersDataSource(context);
        beersDataSource.open();
        List<Beer> beers = beersDataSource.getAll();
        beersDataSource.close();
        return beers;
    }
}
